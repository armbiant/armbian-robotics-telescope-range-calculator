Telescope range calculator
------------------

This tool computes the size of a spherical cupola in which a telescope and its mount can fit.

Currently, the tool is only command line, work is in progress to display the shape of the mount and telescope in 3D, using [Pict3D](https://docs.racket-lang.org/pict3d/index.html) for [Racket](https://racket-lang.org/).

The program is made in [scheme](https://en.wikipedia.org/wiki/Scheme_%28programming_language%29). It defines a scene graph from measurements inputs of the user and animates the mount to compute the bounding sphere's radius. An offset can be given for the centre of the sphere. The measurements have to be put in the beginning of the `telescope_range.ss` file.

The `matrix.ss` file contains definitions for a vector-based 4x4 matrix use in 3D transformations in scheme.
